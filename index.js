var Slack, _, autoMark, autoReconnect, slack, token, channel;

Slack = require('slack-client');
_ = require('lodash');

token = 'xoxb-11961403207-0BLrdwiMEVsSFT5SGUOsBN7z';
autoReconnect = true;
autoMark = true;
slack = new Slack(token, autoReconnect, autoMark);

FOOD_OPTIONS = [
  { reaction: "rice", name: "thai" },
  { reaction: "sushi", name: "sushi" },
  { reaction: "curry", name: "Satkar" },
  { reaction: "pizza", name: "pizza" },
  { reaction: "horse", name: "kebab" },
  { reaction: "beer", name: "Bruuveri" },
  { reaction: "bento", name: "Fafa's" }
];

var addReactions = function(history) {
  var msg = _.findLast(history.messages);
  _.forEach(FOOD_OPTIONS, function(opt) {
    slack._apiCall('reactions.add', {agent: 'node-slack', name: opt.reaction, channel: channel.id, timestamp: msg.ts});
  });
};

slack.on('open', function() {
  var startTime = Math.floor(Date.now() / 1000);
  channel = slack.getChannelGroupOrDMByName("lounas");
  channel.send("Lounasäänestys:\n" + _.map(FOOD_OPTIONS, function(opt) { return ":" + opt.reaction + ": " + opt.name; }).join(", "));
  slack._apiCall('channels.history', {agent: 'node-slack', channel: channel.id, oldest: startTime}, addReactions);
});

slack.on('error', function(error) {
  return console.error("Error: " + error);
});

slack.login();
